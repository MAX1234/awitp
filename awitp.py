import sys
import os
sys.path.insert(0, os.path.dirname(__file__))
import urllib
import cgi
import datetime
import md5
import re

from google.appengine.api import users
from google.appengine.ext import ndb

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

MENTION = re.compile("@([a-zA-Z_0-9.]+)(@[a-zA-Z0-9.-]+\\.[a-zA-Z]+)?")

# Todo:
#     + Timezones
#     + Fix likes (1/(person x post))
#     + More hubs
#     + Proper removal of parks
#     + Link parks to other hubs
#     + Dogs
#     = Park Listing
#
#
# Importance descends, order of doing ascends


class Page():
    title = ""
    html = ""
    link = ""

    def __init__(self, t, h, l):
        self.title = t
        self.html = h
        self.link = l

class Notif(): # will be turned into datastore
    text = ""

    def __init__(self, t):
        self.text = t

pages = [Page('Home', '<i class="fa fa-home w3-margin-right"></i>', '/'), Page('Global', '<i class="fa fa-globe"></i>', '/global'), Page('Profile', '<i class="fa fa-user"></i>', '/profile'), Page('Messages', '<i class="fa fa-envelope"></i>', '/messages')]

class Park(ndb.Model):
    name = ndb.StringProperty(indexed=True)
    code = ndb.StringProperty(indexed=True)
    location = ndb.StringProperty(indexed=True)

def post_key(user_id):
    return ndb.Key('Post', user_id)

def userdata_key(email):
    return ndb.Key('Userdata', email)

class Userdata(ndb.Model):
    name = ndb.StringProperty(indexed=True)
    birthday = ndb.DateProperty(indexed=True)
    location = ndb.StringProperty(indexed=True)
    email = ndb.StringProperty(indexed=True)
    parks = ndb.StructuredProperty(Park, indexed=True, repeated=True)

def dog_key(owner_id):
    return ndb.Key('Dog', owner_id)

class Dog(ndb.Model):
    name = ndb.StringProperty(indexed=True)
    description = ndb.StringProperty(indexed=True)
    breed = ndb.StringProperty(indexed=True)
    avatar = ndb.TextProperty()

def park_key(name):
    return ndb.Key('Park', name)


class Post(ndb.Model):
    html = ndb.StringProperty(indexed=True)
    hub = ndb.StringProperty(indexed=True)
    owner_name = ndb.StringProperty(indexed=True)
    date = ndb.DateTimeProperty(indexed=True, auto_now_add=True)
    owner_email = ndb.StringProperty(indexed=True)
    likes = ndb.IntegerProperty(indexed=True)
    dislikes = ndb.IntegerProperty(indexed=True)
    person = ndb.StringProperty(indexed=True)

def comment_key(user_id):
    return ndb.Key('Comment', user_id)

class Comment(ndb.Model):
    owning_post = ndb.StructuredProperty(Post, indexed=True)
    owner_name = ndb.StringProperty(indexed=True)
    date = ndb.DateTimeProperty(indexed=True, auto_now_add=True)
    owner_email = ndb.StringProperty(indexed=True)
    likes = ndb.IntegerProperty(indexed=True)

def notif_key(user_id):
    return ndb.Key('Notif', user_id)

class Notif(ndb.Model):
    text = ndb.StringProperty(indexed=True)
    date = ndb.DateTimeProperty(indexed=True, auto_now_add=True)
    From = ndb.StringProperty(indexed=True)

class Age():
    months = 0
    years = 0
    def __init__(self, y, m):
        self.years = y
        self.months = m

class MainPage(webapp2.RequestHandler):
    
    def get(self):
        #if self.request.get('hub') == '': # make just for valid ones
        #    self.redirect('/?hub=rcp')
            
        try:
            posts_query = Post.query(
                Post.hub == self.request.get('hub')
                )
            posts = posts_query.fetch()
        except:
            posts = []

        user = users.get_current_user()

        isreg = False

        if user:
            userdata_query = Userdata.query(Userdata.email == user.email())
            mpusers = userdata_query.fetch()

            if len(mpusers) > 0:
                mpuser = mpusers[0]
                isreg = True
            else:
                mpuser = Userdata(name='Someone', birthday=datetime.date(1990, 1, 1), location='Unknown', email=user.email(), parks=[])
                mpuser.put()
        
        template_values = {
            'pages': pages,
            'page': pages[0].title,
            'notifs': [],
            'posts': posts,
            'len': len,
            'md5': md5.new,
            'user': user,
            'url': users.create_login_url('/') if not users.get_current_user() else users.create_logout_url('/'),
            'isreg': isreg,
            'q': urllib.quote_plus,
        }

        if user:
            template_values['name'] = mpuser.name
            template_values['birthday'] = mpuser.birthday
            template_values['location'] = mpuser.location
            template_values['parks'] = mpuser.parks

            notif_query = Notif.query(ancestor=notif_key(user.email()))
            notifs = notif_query.fetch()

            template_values['notifs'] = notifs

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))

class MakePost(webapp2.RequestHandler):
    def post(self):
        user = users.get_current_user()

        userdata_query = Userdata.query(Userdata.email == user.email())
        mpusers = userdata_query.fetch()

        mpuser = mpusers[0]

        if user:
            post = Post(html='<p>' + self.request.get('post') + '</p>', owner_name=mpuser.name, owner_email=user.email(), likes=0, dislikes=0, hub=self.request.get('prev'), person=mpuser.name) # fix hub
            ## Regex for @, add to inbax for dat user

            mentions = re.findall(MENTION, self.request.get('post'))

            self.response.write(mentions)

            for i in mentions:
                if len(i) == 1:
                    notif = Notif(text="{0} mentioned you!".format(user.email()), From=user.email(), key=notif_key(i[0] + "@gmail.com"))
                else:
                    notif = Notif(text="{0} mentioned you!".format(user.email()), From=user.email(), key=notif_key(i[0] + i[1]))

                notif.put()
            
            post.put()
            self.redirect('/?hub=' + self.request.get('prev'))
        else:
            self.redirect('/?hub=' + self.request.get('prev'))

class LikePost(webapp2.RequestHandler):
    def post(self):
        u = users.get_current_user()

        if u:
            post = ndb.Key(urlsafe=self.request.get('id')).get()
            if self.request.get('like'):
                post.likes += 1
            elif self.request.get('dislike'):
                post.dislikes += 1
            post.put()
            self.redirect('/')
        else:
            self.redirect('/')

class DataJS(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('data.js')
        self.response.write(template.render({}))

class EditUserData(webapp2.RequestHandler):
    def post(self):
        user = users.get_current_user()

        if user:
            userdata_query = Userdata.query(Userdata.email == user.email())
            mpusers = userdata_query.fetch()

            mpuser = mpusers[0]

            mpuser.name = self.request.get('name')
            mpuser.birthday = datetime.datetime.strptime(self.request.get('birthday'), '%Y-%m-%d').date()
            mpuser.location = self.request.get('location')
            mpuser.parks = []

            mpuser.put()

class AddParkToUser(webapp2.RequestHandler):
    def post(self):
        user = users.get_current_user()

        if user:
            userdata_query = Userdata.query(Userdata.email == user.email())
            mpusers = userdata_query.fetch()

            mpuser = mpusers[0]

            mpuser.parks.append(ndb.Key(urlsafe=self.request.get('id')).get())

            mpuser.put()

class ParkRegistry(webapp2.RequestHandler):
    def get(self):
        global pages
        
        user = users.get_current_user()
        if user:
            mod = (user.email() == 'maxssteinberg@gmail.com')
        else:
            mod = False
            
        park_query = Park.query()
        parks = park_query.fetch()

        template = JINJA_ENVIRONMENT.get_template('parks.html')
        self.response.write(template.render({'parks': parks, 'mod': mod, 'user': user, 'pages': pages, 'notifs': [], 'len': len}))

class Dogs(webapp2.RequestHandler):
    def get(self):
        global pages
        
        user = users.get_current_user()
        if user:
            mod = (user.email() == 'maxssteinberg@gmail.com')
            dog_query = Dog.query(ancestor=dog_key(user.email()))
            dogs = dog_query.fetch()
        else:
            mod = False
            dogs = []

        template = JINJA_ENVIRONMENT.get_template('profile.html')
        self.response.write(template.render({'dogs': dogs, 'mod': mod, 'user': user, 'pages': pages, 'notifs': [], 'len': len}))

class AddPark(webapp2.RequestHandler):
    def post(self):
        if users.get_current_user().email() != 'maxssteinberg@gmail.com':
            self.response.set_status(401)
            return;
        
        park = Park()

        park.name = self.request.get('name')

        park.location = self.request.get('location')

        park.code = self.request.get('code')

        park.put()

        self.redirect('/global')

class AddDog(webapp2.RequestHandler):
    def post(self):
        dog = Dog(key=dog_key(users.get_current_user().email()))

        dog.name = self.request.get('name')

        dog.description = self.request.get('description')

        dog.breed = self.request.get('breed')

        dog.avatar = self.request.get('image')

        dog.put()

        self.redirect('/profile')

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/post', MakePost),
    ('/like', LikePost),
    ('/data.js', DataJS),
    ('/datamod', EditUserData),
    ('/addpark', AddParkToUser),
    ('/global', ParkRegistry),
    ('/createpark', AddPark),
    ('/profile', Dogs),
    ('/adddog', AddDog),
], debug=True)
